# Pokeapi
<img src="/src/assets/final_fight.png" alt="banner" width="450"/>

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![heroku](https://img.shields.io/badge/%E2%86%91_Deploy_to-Heroku-7056bf.svg?style=flat)](https://pokeapi-jaklip.herokuapp.com)
> Pokeapi Angular assignment

## Table of Contents

- [Install](#install)
- [Develop](#develop)
- [Build](#build)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)
- [Further help](#further-help)

## Install
```
npm install
```

## Develop
```
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
```

## Build
```
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
```
## Maintainers

[Philip Aubert (@pauberexperis)](https://gitlab.com/pauberexperis)
[Jakob Hauge Moe (@JakMoe)](https://gitlab.com/jakmoe)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

Experis © 2020 Noroff Accelerate AS

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

