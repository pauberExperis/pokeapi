import { Component, OnInit } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppService } from './app.service';
import { Pokemon } from './pokemon';
import { ApiResponse } from './apiresponse';
import { PokemonlistComponent } from './pokemonlist/pokemonlist.component'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [AppService]
})
export class AppComponent {
  title = 'pokeapi';

  constructor(private service: AppService) {  }

}
