import { DomElementSchemaRegistry } from '@angular/compiler';

export interface Pokemon {
    name:string
    sprites: Sprites
    weight:number
    height:number
    abilities: []
    base_experience: number
    moves: [Moves]
    types: []

}
interface Moves{
    move: Move
}
interface Move {
    name: string
    url: string
}
interface Sprites {
    back_default: any
    front_default: any
    back_shiny: any
    front_shiny: any
}
