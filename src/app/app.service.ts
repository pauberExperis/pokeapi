import { Injectable } from '@angular/core';
import { Observable, of} from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

import { Pokemon } from './pokemon'
import { ApiResponse } from './apiresponse';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  offset : number = 0;
  pokeDex : [];
  pokedexUrl : string = 'https://pokeapi.co/api/v2/pokemon/?limit=99999';
  pokeUrl : string = 'https://pokeapi.co/api/v2/pokemon';
  spriteUrl : string = 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/';
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  constructor(private http: HttpClient) {
    
   }
   async getApiResponse() {
    const asyncResult = await this.http.get<ApiResponse>(this.pokedexUrl).toPromise()
     return asyncResult;
      }

    getPokemon(name: string): Observable<Pokemon> {
      const url = `${this.pokeUrl}/${name}`;
      return this.http.get<Pokemon>(url).pipe(tap(_ => catchError(this.handleError<Pokemon>(`getPokemon name=${name}`))
      ));
    }

    private handleError<T> (operation = 'operation', result?: T) {
      return (error: any): Observable<T> => {
        console.error(error);
  
        return of(result as T);
      }
    }
}
