import { Component, OnInit, Input } from '@angular/core';
import { Pokemon } from '../pokemon';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { AppService } from '../app.service';

@Component({
  selector: 'app-pokemondetail',
  templateUrl: './pokemondetail.component.html',
  styleUrls: ['./pokemondetail.component.css']
})
export class PokemondetailComponent implements OnInit {

  @Input() pokemon: Pokemon;

  constructor(private route: ActivatedRoute,
    private service: AppService,
    private location: Location) { }

  ngOnInit(): void {
    this.getPokemon();
  }

  getPokemon(): void {
    const name = this.route.snapshot.paramMap.get('name');
    this.service.getPokemon(name).subscribe(pokemon => {this.pokemon = pokemon; if (this.pokemon) 
      {this.pokemon.moves.forEach(move => {move.move.name = move.move.name.replace(/-/g, " "); 
      move.move.name = move.move.name = move.move.name.replace(/^\w/, c=> c.toUpperCase());})}});
   
  }

  goBack(): void {
    this.location.back();
  }

}
