import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { Pokemon } from '../pokemon';
import { ApiResponse } from '../apiresponse';

@Component({
  selector: 'app-pokemonlist',
  templateUrl: './pokemonlist.component.html',
  styleUrls: ['./pokemonlist.component.css']
})
export class PokemonlistComponent implements OnInit {
  pokemons: Pokemon[] = [];
  pokemon: Pokemon;
  apiResponse: ApiResponse;
  spriteUrl: string = 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/'
  constructor(private service: AppService) { }

  ngOnInit() {
    this.getApiResponse();
  }
  async getApiResponse() {
    this.service.getApiResponse().then(apiResponse => this.apiResponse = apiResponse);
    await new Promise(resolve => setTimeout(resolve, 1000));
    this.getPokemons();
  }

  getPokemons() {
    this.apiResponse.results.forEach(Pokemon => this.pokemons.push(Pokemon)); 

  }
  postUrl(final: string): string {
    final = final.replace(/\/$/, '');
    final = final.substring(final.lastIndexOf('/') + 1);
    return final;
  }

  onClick(input:string): void {
    console.log(input);
  }

}
