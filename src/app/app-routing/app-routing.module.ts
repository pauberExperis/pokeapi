import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from '../app.component';
import { PokemondetailComponent } from '../pokemondetail/pokemondetail.component'
import { PokemonlistComponent } from '../pokemonlist/pokemonlist.component';

const routes: Routes = [
  { path: '', redirectTo:'/list', pathMatch: 'full'},
  { path: 'list', component: PokemonlistComponent },
  { path: 'detail/:name', component: PokemondetailComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
